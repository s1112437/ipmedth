import 'package:firstproject/myMeasurements.dart';
import 'package:firstproject/register.dart';
import 'package:firstproject/models/api_response_data.dart';
import 'package:firstproject/succes.dart';
import 'package:firstproject/testAnimation.dart';
import 'package:firstproject/widgets/bodytext.dart';
import 'package:firstproject/widgets/headline1.dart';
import 'package:firstproject/widgets/input.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'hand_opmeten.dart';
import 'models/user.dart';
import 'services/user_service.dart';

class login extends StatefulWidget {
  const login({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<login> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final GlobalKey<FormState> formkeyPassword = GlobalKey<FormState>();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtEmailPasswordForgot = TextEditingController();
  TextEditingController txtPassword = TextEditingController();
  bool loading = false;
  bool loadingPassword = false;

  void _testLogin() async {
    _saveAndRedirectToHome(
        User(id: 1, firstname: "test", lastname: "test", sex: "Male"));
  }

  void _loginUser() async {
    ApiResponse response = await loginUser(txtEmail.text, txtPassword.text);
    if (response.error == null) {
      print('succesvol ingelogd');
      _saveAndRedirectToHome(response.data as User);
    } else {
      setState(() {
        loading = false;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  void _requestNewPassword() async {
    ApiResponse response = await requestPassword(txtEmailPasswordForgot.text);
    if (response.error == null) {
      Navigator.of(context, rootNavigator: true).pop();
      setState(() {
        loadingPassword = false;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Email succesvol verzonden')));
      Future.delayed(const Duration(milliseconds: 10000), () {
        ScaffoldMessenger.of(context).clearSnackBars();
      });
    } else {
      setState(() {
        loadingPassword = !loadingPassword;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  void _saveAndRedirectToHome(User user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('token', user.token ?? '');
    await pref.setString('firstname', user.firstname ?? '');
    await pref.setString('middlename', user.middlename ?? '');
    await pref.setString('lastname', user.lastname ?? '');
    await pref.setString('dateOfBirth', user.dateOfBirth ?? '');
    await pref.setString('email', user.email ?? '');
    await pref.setString('image', user.image ?? '');
    await pref.setString('musicalInstrument', user.musicalInstrument ?? '');
    await pref.setInt('userId', user.id ?? 0);
    Navigator.of(context).pushAndRemoveUntil(
        // MaterialPageRoute(builder: (context) => succes()), (route) => false);
        MaterialPageRoute(builder: (context) => HandOpmeten()),
        (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Center(
          child: Container(
        width: 0.8 * width,
        // height: 0.6 * height,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xFF949494)),
          borderRadius: BorderRadius.circular(5),
          color: Theme.of(context).colorScheme.secondaryVariant,
        ),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 0.25 * width,
                color: Theme.of(context).colorScheme.primaryVariant,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 0.25 * width,
                          decoration: BoxDecoration(
                            color:
                                Theme.of(context).colorScheme.secondaryVariant,
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(20),
                            child: Image(
                                fit: BoxFit.cover,
                                image:
                                    AssetImage('assets/logo_revalidatie.png')),
                          ),
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          text_h1('Welkom terug!', Colors.white),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: bodytext(
                                'Nog geen account? registreer je met de onderstaande knop als musici of iemand anders die zijn hand wil opmeten!',
                                Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                    width: 200,
                                    height: 50,
                                    child: OutlinedButton(
                                      style: ElevatedButton.styleFrom(
                                          padding: EdgeInsets.all(10),
                                          primary: Colors.white,
                                          elevation: 5,
                                          side: const BorderSide(
                                              color: Color(0xFF00AAA6),
                                              width: 2),
                                          textStyle: const TextStyle(
                                            color: Colors.black,
                                            fontSize: 24,
                                          )),
                                      onPressed: () {
                                        Navigator.of(context).pushAndRemoveUntil(
                                            MaterialPageRoute(
                                                builder: (context) => RegisterPage(
                                                    title:
                                                        'Revalidatie Friesland')),
                                            (route) => false);
                                      },
                                      child: Text('Registreer'),
                                    )),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 0.50 * width,
                padding: const EdgeInsets.all(40.0),
                // color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    text_h1("Log in", Colors.black),
                    Container(
                      margin: const EdgeInsets.only(top: 20.0),
                      child: Form(
                          key: formkey,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextFormField(
                                  style: TextStyle(color: Colors.black),
                                  keyboardType: TextInputType.emailAddress,
                                  controller: txtEmail,
                                  validator: (val) => val!.isEmpty
                                      ? 'Geen valide email adres'
                                      : null,
                                  decoration: kInputDecoration(
                                      'Email', Icon(Icons.email)),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                TextFormField(
                                  obscureText: true,
                                  controller: txtPassword,
                                  validator: (val) => val!.length < 6
                                      ? 'Minimaal 6 karakters benodigd'
                                      : null,
                                  decoration: kInputDecoration(
                                      'Wachtwoord', Icon(Icons.lock)),
                                ),
                                TextButton(
                                    onPressed: () {
                                      showDialog<void>(
                                        context: context,
                                        barrierDismissible:
                                            true, // user must tap button!
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: text_h1(
                                                'Wachtwoord vergeten',
                                                Colors.black),
                                            content: SingleChildScrollView(
                                              child: Form(
                                                key: formkeyPassword,
                                                child: Column(
                                                  children: [
                                                    bodytext(
                                                        'Vul onderstaand uw email in. Wanneer op verzend is gedrukt kunt u in de mail uw wachtwoord wijzigen.',
                                                        Colors.black),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    TextFormField(
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                      keyboardType:
                                                          TextInputType
                                                              .emailAddress,
                                                      controller:
                                                          txtEmailPasswordForgot,
                                                      validator: (val) => val!
                                                              .isEmpty
                                                          ? 'Geen valide email adres'
                                                          : null,
                                                      decoration:
                                                          kInputDecoration(
                                                              'Email',
                                                              Icon(
                                                                  Icons.email)),
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    loadingPassword
                                                        ? Center(
                                                            child:
                                                                CircularProgressIndicator(),
                                                          )
                                                        : Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: [
                                                              ElevatedButton(
                                                                  style: ElevatedButton.styleFrom(
                                                                      elevation:
                                                                          8,
                                                                      minimumSize: Size(
                                                                          130,
                                                                          35),
                                                                      onPrimary: Theme.of(context)
                                                                          .colorScheme
                                                                          .primaryVariant,
                                                                      primary:
                                                                          Colors
                                                                              .white),
                                                                  onPressed:
                                                                      Navigator.of(context)
                                                                          .pop,
                                                                  child: Text(
                                                                      'Sluit',
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          fontSize:
                                                                              15))),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              ElevatedButton(
                                                                  style: ElevatedButton.styleFrom(
                                                                      elevation:
                                                                          8,
                                                                      minimumSize:
                                                                          Size(
                                                                              130,
                                                                              35),
                                                                      onPrimary:
                                                                          Colors
                                                                              .white,
                                                                      primary: Theme.of(
                                                                              context)
                                                                          .colorScheme
                                                                          .primaryVariant),
                                                                  onPressed:
                                                                      () {
                                                                    if (formkeyPassword
                                                                        .currentState!
                                                                        .validate()) {
                                                                      setState(
                                                                          () {
                                                                        loadingPassword =
                                                                            !loadingPassword;
                                                                        _requestNewPassword();
                                                                      });
                                                                    }
                                                                  },
                                                                  child: Text(
                                                                      'Verzend',
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          fontSize:
                                                                              15))),
                                                            ],
                                                          ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    child: Text('Wachtwoord vergeten')),
                                // SizedBox(
                                //   height: 20,
                                // ),
                                loading
                                    ? Center(
                                        child: CircularProgressIndicator(),
                                      )
                                    : ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            elevation: 8,
                                            minimumSize: Size(170, 50),
                                            onPrimary: Colors.white,
                                            primary: Theme.of(context)
                                                .colorScheme
                                                .primaryVariant),
                                        onPressed: () {
                                          // _testLogin();
                                          if (formkey.currentState!
                                              .validate()) {
                                            setState(() {
                                              loading = true;
                                              _loginUser();
                                            });
                                          }
                                        },
                                        child: Text('Login',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20)))
                              ])),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
