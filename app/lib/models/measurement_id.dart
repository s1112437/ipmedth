class Measurement {
  int? id;

  Measurement({
    this.id,
  });

  //json data to user model
  factory Measurement.fromJson(Map<String, dynamic> json) {
    return Measurement(id: json['measurement']['id']);
  }
}
