class User {
  int? id;
  String? firstname;
  String? middlename;
  String? lastname;
  String? sex;
  String? musicalInstrument;
  String? dateOfBirth;
  String? email;
  String? emailDoctor;
  String? image;
  // String? password;
  String? token;

  User(
      {this.id,
      this.firstname,
      this.middlename,
      this.lastname,
      this.sex,
      this.musicalInstrument,
      this.dateOfBirth,
      this.email,
      this.emailDoctor,
      this.image,
      this.token});

  //json data to user model
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['user']['id'],
      firstname: json['user']['firstname'],
      middlename: json['user']['middlename'],
      lastname: json['user']['lastname'],
      sex: json['user']['sex'],
      musicalInstrument: json['user']['musical_instrument'],
      dateOfBirth: json['user']['date_of_birth'],
      email: json['user']['email'],
      emailDoctor: json['user']['email_doctor'],
      image: json['user']['avatar_image'],
      token: json['token'],
    );
  }
}
