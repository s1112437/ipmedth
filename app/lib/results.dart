import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firstproject/hand_opmeten.dart';
import 'package:firstproject/secondary_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firstproject/Measurement.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:firstproject/services/user_service.dart';

import 'package:firstproject/constant.dart';

import 'myMeasurements.dart';

class ScreenArguments {
  final int measurementId;

  ScreenArguments(this.measurementId);
}

Future<Measurement> getMeasurement(int measurementId) async {
  String token = await getToken();
  final response = await http.post(Uri.parse(measurementURL),
      headers: <String, String>{
        'content-type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(<String, int>{'measurementId': measurementId}));

  if (response.statusCode == 200) {
    return Measurement.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Fout bij laden van metingen');
  }
}

Future<http.Response> deleteMeasurement(
    int measurementId, BuildContext context) async {
  String token = await getToken();
  final http.Response response = await http.delete(Uri.parse(measurementURL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(<String, int>{'measurementId': measurementId}));

  if (json.decode(response.body)['status'] == 1) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyMeasurements()));
  } else {
    showError(context);
  }

  return response;
}

void showError(BuildContext context) async {
  await Future.delayed(Duration(microseconds: 1));
  showDialog<String>(
    context: context,
    builder: (BuildContext context) => AlertDialog(
        title: Text("Fout bij verwijderen meting"),
        content:
            Text('Er is een fout opgetreden bij het verwijderen van uw meting'),
        actions: <Widget>[
          FlatButton(
            // FlatButton widget is used to make a text to work like a button
            textColor: Colors.red,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyMeasurements()));
            },
            child: const Text(
                'SLUITEN'), // function used to perform after pressing the button
          ),
        ]),
  );
}

class Results extends StatefulWidget {
  const Results({Key? key}) : super(key: key);

  static const routeName = '/results';

  @override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Results> {
  bool _isloaded = false;
  late Future<Measurement> futureMeasurement;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    futureMeasurement = getMeasurement(args.measurementId);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        foregroundColor: Color(0xff009995),
        backgroundColor: Colors.white,
        toolbarHeight: 80,
        actions: <Widget>[
          Image.asset(
            'assets/logo_revalidatie.png',
            fit: BoxFit.contain,
            height: 60,
          ),
          Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
        ],
        elevation: 0,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width * 0.7,
            margin: const EdgeInsets.only(top: 40.0, bottom: 40.0),
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Color(0xFFF6F6F6),
              borderRadius: BorderRadius.circular(5),
              border: Border.all(width: 1.0, color: Color(0xFF949494)),
            ),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(
                      bottom: 2.0, left: 10.0, right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Gegevens inzien',
                          style: TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.w600,
                              fontFamily: GoogleFonts.openSans().fontFamily)),
                      ButtonTheme(
                          minWidth: 177,
                          height: 60,
                          child: OutlinedButton(
                              onPressed: () {
                                showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      AlertDialog(
                                    title: const Text("Meting verwijderen"),
                                    content: const Text(
                                        "Weet u zeker dat u de meting wilt verwijderen?"),
                                    actions: <Widget>[
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              primary: Color(0xff009995)),
                                          onPressed: () => Navigator.pop(
                                              context, 'Annuleren'),
                                          child: const Text('Annuleren')),
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              primary: Colors.red),
                                          onPressed: () => {
                                                deleteMeasurement(
                                                    args.measurementId,
                                                    context),
                                              },
                                          child: const Text('Verwijderen')),
                                    ],
                                  ),
                                );
                              },
                              child: Text("Verwijderen",
                                  style: TextStyle(color: Colors.red)),
                              style: OutlinedButton.styleFrom(
                                  side: BorderSide(color: Colors.red),
                                  primary: Colors.redAccent))),
                    ],
                  ),
                ),
                SizedBox(
                  // height: 380,
                  width: MediaQuery.of(context).size.width * 1,
                  child: FutureBuilder<Measurement>(
                      future: futureMeasurement,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          Measurement? measurement = snapshot.data;
                          return DataTable(columns: const <DataColumn>[
                            DataColumn(label: Text('Lengte')),
                            DataColumn(label: Text('Resultaat')),
                          ], rows: <DataRow>[
                            DataRow(
                              cells: <DataCell>[
                                DataCell(Text('Span 1-2')),
                                DataCell(Text(
                                    measurement!.span_1_2.toString() + 'mm'))
                              ],
                            ),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 1-3')),
                              DataCell(
                                  Text(measurement.span_1_3.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 1-4')),
                              DataCell(
                                  Text(measurement.span_1_4.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              const DataCell(Text('Span 1-5')),
                              DataCell(
                                  Text(measurement.span_1_5.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 2-3')),
                              DataCell(
                                  Text(measurement.span_2_3.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 2-4')),
                              DataCell(
                                  Text(measurement.span_2_4.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 2-5')),
                              DataCell(
                                  Text(measurement.span_2_5.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 3-4')),
                              DataCell(
                                  Text(measurement.span_3_4.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 3-5')),
                              DataCell(
                                  Text(measurement.span_3_5.toString() + 'mm'))
                            ]),
                            DataRow(cells: <DataCell>[
                              DataCell(Text('Span 4-5')),
                              DataCell(
                                  Text(measurement.span_4_5.toString() + 'mm'))
                            ]),
                            // DataRow(cells: <DataCell>[
                            //   DataCell(Text('Hoogte verschil 1-3')),
                            //   DataCell(Text(
                            //       measurement.height_difference_1_3.toString() +
                            //           'mm'))
                            // ]),
                            // DataRow(cells: <DataCell>[
                            //   DataCell(Text('Hoogte verschil 3-5')),
                            //   DataCell(Text(
                            //       measurement.height_difference_3_5.toString() +
                            //           'mm'))
                            // ]),
                            // DataRow(cells: <DataCell>[
                            //   DataCell(Text('Handbreedte')),
                            //   DataCell(Text(
                            //       measurement.hand_width.toString() + 'mm'))
                            // ]),
                            // DataRow(cells: <DataCell>[
                            //   DataCell(Text('Handlengte')),
                            //   DataCell(Text(
                            //       measurement.hand_length.toString() + 'mm'))
                            // ])
                          ], dataRowHeight: 23);
                        } else if (snapshot.hasError) {
                          return AlertDialog(
                              title: Text("Fout bij laden meting"),
                              content: Text(
                                  'Er is een fout opgetreden bij het ophalen van uw meting'),
                              actions: [
                                FlatButton(
                                  // FlatButton widget is used to make a text to work like a button
                                  textColor: Colors.red,
                                  onPressed:
                                      () {}, // function used to perform after pressing the button
                                  child: Text('SLUITEN'),
                                ),
                              ]);
                        }
                        return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(
                                  color: Color(0xFF00AAA6))
                            ]);
                      }),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SecondaryButton(
                          buttonText: "Mijn gegevens",
                          buttonFunction: "mijn gegevens",
                          buttonHeight: 50,
                          buttonWidth: 220)
                      // OutlinedButton(
                      //     onPressed: () {
                      //       Navigator.push(context,
                      //           MaterialPageRoute(builder: (context) {
                      //         return (MyMeasurements());
                      //       }));
                      //     },
                      //     child: Text("Mijn Gegevens",
                      //         style: TextStyle(color: Colors.black)),
                      //     style: OutlinedButton.styleFrom(
                      //       side: BorderSide(
                      //         color: Color(0xff009995),
                      //       ),
                      //       primary: Color(0xff009995),
                      //     )),
                    ],
                  )),
            ],
          )
        ],
      )),
    );
  }
}
