import 'package:flutter/material.dart';
import 'package:firstproject/secondary_button.dart';
import 'package:firstproject/primary_button.dart';
import 'dart:math' as math;

import 'widgets/nav.dart'; // import this

class HandSelectTestomgeving extends StatelessWidget {
  const HandSelectTestomgeving({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: nav(),
        appBar: AppBar(
            foregroundColor: Color(0xff009995),
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            actions: <Widget>[
              Image.asset(
                'assets/logo_revalidatie.png',
                fit: BoxFit.contain,
                height: 60,
              ),
              Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
            ],
            elevation: 0,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  iconSize: 50,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            )),
        body: Column(children: [
          Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.08),
              padding: EdgeInsets.all(40.0),
              decoration: BoxDecoration(
                color: const Color(0xFFF6F6F6),
                border: Border.all(color: const Color(0xFF949494)),
                borderRadius: BorderRadius.circular(5),
              ),
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.62,
                  maxWidth: MediaQuery.of(context).size.width * 0.8),
              child: Column(
                children: [
                  Expanded(
                    flex: 7,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: Text(
                            "Hand opmeten (testomgeving)",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        SizedBox(
                            width: 800,
                            child: Text(
                              "Welke hand wilt U gaan opmeten?",
                              style: Theme.of(context).textTheme.headline3,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 10,
                            )),
                        Container(
                            margin: EdgeInsets.only(top: 25, bottom: 25),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(children: [
                                    Image(
                                      image: AssetImage(
                                          'assets/hand_inscannen.png'),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.2,
                                      width:
                                          MediaQuery.of(context).size.height *
                                              0.2,
                                    ),
                                    SizedBox(height: 20),
                                    PrimaryButton(
                                        buttonText: "Links",
                                        buttonFunction: "Links testomgeving",
                                        buttonHeight: 50,
                                        buttonWidth: 150)
                                  ]),
                                  Column(children: [
                                    Transform(
                                      alignment: Alignment.center,
                                      transform: Matrix4.rotationY(math.pi),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/hand_inscannen.png'),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.2,
                                        width:
                                            MediaQuery.of(context).size.height *
                                                0.2,
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    PrimaryButton(
                                        buttonText: "Rechts",
                                        buttonFunction: "Rechts testomgeving",
                                        buttonHeight: 50,
                                        buttonWidth: 150)
                                  ])
                                ]))
                      ],
                    ),
                  ),
                ],
              )),
          Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.height * 0.15,
                  right: MediaQuery.of(context).size.height * 0.15,
                  top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SecondaryButton(
                      buttonText: "Terug",
                      buttonFunction: "terug",
                      buttonHeight: 50,
                      buttonWidth: 150)
                ],
              ))
        ]));
  }
}
