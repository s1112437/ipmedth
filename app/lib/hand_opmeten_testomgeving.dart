import 'package:flutter/material.dart';
import 'package:firstproject/secondary_button.dart';
import 'package:firstproject/primary_button.dart';

import 'widgets/nav.dart';

class HandOpmetenTestomgeving extends StatelessWidget {
  const HandOpmetenTestomgeving({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: nav(),
        appBar: AppBar(
            foregroundColor: Color(0xff009995),
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            actions: <Widget>[
              Image.asset(
                'assets/logo_revalidatie.png',
                fit: BoxFit.contain,
                height: 60,
              ),
              Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
            ],
            elevation: 0,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  iconSize: 50,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            )),
        body: Column(children: [
          Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.08),
              padding: EdgeInsets.all(40.0),
              decoration: BoxDecoration(
                color: const Color(0xFFF6F6F6),
                border: Border.all(color: const Color(0xFF949494)),
                borderRadius: BorderRadius.circular(5),
              ),
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.6,
                  maxWidth: MediaQuery.of(context).size.width * 0.8),
              child: Row(
                children: [
                  Expanded(
                    flex: 7,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: Text(
                            "Hand opmeten (testomgeving)",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        SizedBox(
                            width: 800,
                            child: Text(
                              "In de volgende stap gaat u de hand opmeten in de testomgeving om bekend te raken met de manier van het meten. Let erop dat u bij het meten de hand volledig strekt. U kunt de vingers ook buiten het meetveld neerzetten",
                              style: Theme.of(context).textTheme.headline3,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 10,
                            ))
                      ],
                    ),
                  ),
                  const Expanded(
                      flex: 3,
                      child: Image(
                        image: AssetImage('assets/hand_inscannen.png'),
                      ))
                ],
              )),
          Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.height * 0.15,
                  right: MediaQuery.of(context).size.height * 0.15,
                  top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SecondaryButton(
                      buttonText: "Terug",
                      buttonFunction: "terug",
                      buttonHeight: 50,
                      buttonWidth: 150),
                  PrimaryButton(
                      buttonText: "Volgende",
                      buttonFunction: "volgende hand select testomgeving",
                      buttonHeight: 50,
                      buttonWidth: 150)
                ],
              ))
        ]));
  }
}
