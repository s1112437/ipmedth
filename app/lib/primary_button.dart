import 'package:firstproject/hand_opmeten.dart';
import 'package:firstproject/hand_select.dart';
import 'package:firstproject/hand_select_testomgeving.dart';
import 'package:firstproject/pages/measure.dart';
import 'package:firstproject/pages/measure_testomgeving.dart';
import 'package:firstproject/results.dart';
import 'package:firstproject/services/hand.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PrimaryButton extends StatelessWidget {
  PrimaryButton(
      {required this.buttonText,
      required this.buttonFunction,
      required this.buttonHeight,
      required this.buttonWidth});

  String buttonText;
  String buttonFunction;
  double buttonHeight;
  double buttonWidth;

  var handObjectList = [
    Hand("", "Leg steeds de twee fingertoppen neer die worden aangegeven", 0.0),
    Hand("span_1-2", "Afstand duim en wijsvinger (span 1-2)", 0.0),
    Hand("span_1-3", "Afstand duim en middelvinger (span 1-3)", 0.0),
    Hand("span_1-4", "Afstand duim en ringvinger (span 1-4)", 0.0),
    Hand("span_1-5", "Afstand duim en pinkvinger (span 1-5)", 0.0),
    Hand("span_2-3", "Afstand wijsvinger en middelvinger (span 2-3)", 0.0),
    Hand("span_2-4", "Afstand wijsvinger en ringvinger (span 2-4)", 0.0),
    Hand("span_2-5", "Afstand wijsvinger en pinkvinger (span 2-5)", 0.0),
    Hand("span_3-4", "Afstand middelvinger en ringvinger (span 3-4)", 0.0),
    Hand("span_3-5", "Afstand middelvinger en pinkvinger (span 3-5)", 0.0),
    Hand("span_4-5", "Afstand ringing en pinkvinger (span 4-5)", 0.0),
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: buttonWidth,
        height: buttonHeight,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(10),
              primary: Color(0xFF00AAA6),
              elevation: 5,
              side: const BorderSide(color: Color(0xFF00AAA6), width: 2),
              textStyle: const TextStyle(
                color: Colors.black,
                fontSize: 24,
              )),
          onPressed: () {
            buttonHandler(buttonFunction, context);
          },
          child: Text(buttonText),
        ));
  }

  Future<void> buttonHandler(String action, BuildContext context) async {
    if (action == "volgende hand select") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (const HandSelect());
      }));
    }
    if (action == "volgende hand select testomgeving") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (const HandSelectTestomgeving());
      }));
    }
    if (action == "Rechts") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (MeasurePage(
          hand: "rechts",
          step: 1,
          handList: handObjectList,
        ));
      }));
    }
    if (action == "Links") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (MeasurePage(hand: "links", step: 1, handList: handObjectList));
      }));
    }
    if (action == "Rechts testomgeving") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (MeasurePageTestomgeving(
          hand: "rechts",
          step: 1,
          handList: handObjectList,
        ));
      }));
    }
    if (action == "Links testomgeving") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (MeasurePageTestomgeving(
            hand: "links", step: 1, handList: handObjectList));
      }));
    }
    if (action == "Terug naar start") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (HandOpmeten());
      }));
    }

    if (action == "naar website") {
      var url = 'https://ipmedth-groep4-web.herokuapp.com/';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        print('launchen werkt niet');
      }
    }
  }
}
