import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firstproject/myMeasurements.dart';
import 'package:firstproject/pages/measure.dart';
import 'package:firstproject/results.dart';
import 'package:firstproject/palette.dart';
import 'package:firstproject/hand_opmeten.dart';
import 'package:firstproject/hand_opmeten_testomgeving.dart';
import 'package:firstproject/hand_select.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/services.dart';
import 'package:firstproject/testAnimation.dart';

import 'login.dart';
import 'pages/splash.dart';

void main() {
  // runApp(MaterialApp(initialRoute: '/', routes: {
  //   '/': (context) => const MyHomePage(title: "Home"),
  //   '/myMeasurements': (context) => const MyMeasurements(),
  //   Results.routeName: (context) => const Results(),
  //   '/testanimation': (context) => const TestAnimation(),
  // }));
  runApp(MaterialApp(home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    final ColorScheme colorScheme = ColorScheme(
      primary: Color(0xff009995),
      primaryVariant: Color(0xff009995),
      secondary: Color(0xFF000000),
      secondaryVariant: Color(0xFFF6F6F6),
      background: Colors.white,
      surface: Colors.white,
      onBackground: Colors.white,
      error: Colors.red,
      onError: Colors.red,
      onPrimary: Colors.white,
      onSecondary: Color(0xFFFFFFFF),
      onSurface: Color(0xFF241E30),
      brightness: Brightness.light,
    );

    final text_color = Colors.black;

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          fontFamily: GoogleFonts.openSans().fontFamily,
          primarySwatch: Palette.kToDark,
          colorScheme: colorScheme,
          textTheme: const TextTheme(
            headline1: TextStyle(
                fontSize: 42.0,
                fontWeight: FontWeight.bold,
                color: Colors.black),
            headline2: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
            headline3: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            bodyText1: TextStyle(fontSize: 15.0, color: Colors.black),
          )),
      home: Splash(),
      routes: {Results.routeName: (context) => const Results()},
    );
  }
}
