import 'package:firstproject/register.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/widgets/headline1.dart';
import 'package:firstproject/widgets/nav.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';
import 'models/api_response_data.dart';
import 'models/user.dart';

class succes extends StatefulWidget {
  const succes({Key? key}) : super(key: key);

  @override
  State<succes> createState() => _succesState();
}

class _succesState extends State<succes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
      drawer: nav(),
      body: Builder(builder: (context) {
        return Container(
          margin: const EdgeInsets.only(top: 20.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              // text_h1('sdhkshksjdh', Colors.black),
              IconButton(
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  icon: Icon(Icons.menu),
                  color: Theme.of(context).colorScheme.primaryVariant,
                  iconSize: 50)
            ],
          ),
        );
      }),
    );
  }
}
