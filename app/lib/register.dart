import 'dart:core';
import 'dart:core';
import 'dart:io';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/succes.dart';
import 'package:firstproject/widgets/bodytext.dart';
import 'package:firstproject/widgets/headline1.dart';
import 'package:firstproject/widgets/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constant.dart';
import 'hand_opmeten.dart';
import 'login.dart';
import 'models/api_response_data.dart';
import 'models/user.dart';
import 'palette.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  DateTime selectedDate = DateTime.now();
  bool loading = false;
  String btnText = "Volgende stap";
  File? imageFile;
  String? nieuw;
  final picker = ImagePicker();
  // String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(selectedDate);
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  StepState stateButton1 = StepState.editing;
  StepState stateButton2 = StepState.disabled;
  StepState stateButton3 = StepState.disabled;
  TextEditingController _textEditingController = TextEditingController();
  TextEditingController firstNameController = TextEditingController(),
      middleNameController = TextEditingController(),
      lastNameController = TextEditingController(),
      musicalInstrumentController = TextEditingController(),
      emailController = TextEditingController(),
      emailDoctorController = TextEditingController(),
      passwordController = TextEditingController(),
      passwordConfirmController = TextEditingController();
  int _counter = 0;
  int _currentStep = 0;
  var _valueGender;
  bool isChecked = false;
  Color registerColor = Color(0xff009995);

  chooseImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);
    setState(() {
      imageFile = File(pickedFile!.path);
    });
  }

  void _registerUser() async {
    if (getStringImage(imageFile) == null) {
      nieuw = '';
    } else {
      nieuw = getStringImage(imageFile);
    }
    ApiResponse response = await registerUser(
        emailController.text,
        passwordController.text,
        firstNameController.text,
        middleNameController.text,
        lastNameController.text,
        _valueGender,
        musicalInstrumentController.text,
        selectedDate,
        emailDoctorController.text,
        nieuw);
    if (response.error == null) {
      _saveAndRedirectToHome(response.data as User);
    } else {
      setState(() {
        loading = !loading;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  // Save and redirect to home
  void _saveAndRedirectToHome(User user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('token', user.token ?? '');
    await pref.setString('firstname', user.firstname ?? '');
    await pref.setString('middlename', user.middlename ?? '');
    await pref.setString('lastname', user.lastname ?? '');
    await pref.setString('dateOfBirth', user.dateOfBirth ?? '');
    await pref.setString('email', user.email ?? '');
    await pref.setString('musicalInstrument', user.musicalInstrument ?? '');
    await pref.setString('image', user.image ?? '');
    await pref.setInt('userId', user.id ?? 0);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HandOpmeten()),
        (route) => false);
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1950),
      //fix dit
      lastDate: DateTime(2023),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;

        print(selectedDate);
      });
      String formattedDate = DateFormat('dd-MM-yyyy').format(selectedDate);
      _textEditingController.text = '$formattedDate';
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: 0.8 * width,
              // height: 0.8 * height,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xFF949494)),
                borderRadius: BorderRadius.circular(5),
                color: Theme.of(context).colorScheme.secondaryVariant,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: 0.25 * width,
                    color: Theme.of(context).colorScheme.primaryVariant,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 0.25 * width,
                              decoration: BoxDecoration(
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondaryVariant,
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(20),
                                child: Image(
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                        'assets/logo_revalidatie.png')),
                              ),
                            )
                          ],
                        ),
                        Container(
                          height: 0.45 * height,
                          padding: EdgeInsets.only(left: 40, right: 40),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                'Welkom terug!',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 35,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Text(
                                  'Heb jij je al geregistreerd als patiënt of musici die benieuwd was over jou eigen hand? Log dan in via de knop hieronder!',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                        width: 200,
                                        height: 50,
                                        child: OutlinedButton(
                                          style: ElevatedButton.styleFrom(
                                              padding: EdgeInsets.all(10),
                                              primary: Colors.white,
                                              elevation: 5,
                                              side: const BorderSide(
                                                  color: Color(0xFF00AAA6),
                                                  width: 2),
                                              textStyle: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 24,
                                              )),
                                          onPressed: () {
                                            Navigator.of(context).pushAndRemoveUntil(
                                                MaterialPageRoute(
                                                    builder: (context) => login(
                                                        title:
                                                            'Revalidatie Friesland')),
                                                (route) => false);
                                          },
                                          child: Text('Log in'),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        width: 0.50 * width,
                        padding: const EdgeInsets.only(
                            top: 40.0, left: 40, right: 40),
                        // color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            text_h1('Registreer', Colors.black),
                            Container(
                              margin: const EdgeInsets.only(top: 20.0),
                              child: bodytext(
                                  'Nog geen account? Registreer je dan via het onderstaande formulier. Op deze manier kan je metingen van je hand doen en deze terugzien!',
                                  Colors.black),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 20.0),
                              child: Form(
                                  key: formkey,
                                  child: Center(
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.5,
                                      child: Stepper(
                                        controlsBuilder: (BuildContext context,
                                            {VoidCallback? onStepContinue,
                                            VoidCallback? onStepCancel}) {
                                          return loading
                                              ? Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                )
                                              : Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    TextButton(
                                                      onPressed: onStepCancel,
                                                      child: SizedBox(
                                                          width: 140,
                                                          height: 50,
                                                          child: OutlinedButton(
                                                            style: ElevatedButton
                                                                .styleFrom(
                                                                    padding:
                                                                        EdgeInsets.all(
                                                                            10),
                                                                    primary: Colors
                                                                        .white,
                                                                    elevation:
                                                                        5,
                                                                    side: const BorderSide(
                                                                        color: Color(
                                                                            0xFF00AAA6),
                                                                        width:
                                                                            2),
                                                                    textStyle:
                                                                        const TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          24,
                                                                    )),
                                                            onPressed: () {
                                                              onStepCancel;
                                                            },
                                                            child:
                                                                Text('Vorige'),
                                                          )),
                                                    ),
                                                    TextButton(
                                                      onPressed: onStepContinue,
                                                      child: ElevatedButton(
                                                          style: ElevatedButton
                                                              .styleFrom(
                                                                  elevation: 8,
                                                                  minimumSize:
                                                                      Size(170,
                                                                          50),
                                                                  onPrimary: Colors
                                                                      .white,
                                                                  primary:
                                                                      registerColor),
                                                          onPressed:
                                                              onStepContinue,
                                                          child: Text(btnText,
                                                              style: TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      20))),
                                                    ),
                                                  ],
                                                );
                                        },
                                        steps: _mySteps(),
                                        type: StepperType.horizontal,
                                        currentStep: this._currentStep,
                                        onStepTapped: (step) {
                                          setState(() {
                                            this._currentStep = step;
                                          });
                                          if (this._currentStep == 0 ||
                                              this._currentStep == 1) {
                                            setState(() {
                                              this.btnText = 'Volgende stap';
                                              registerColor = Color(0xff009995);
                                            });
                                          } else {
                                            setState(() {
                                              this.btnText = 'Registreer';
                                            });
                                          }

                                          if (this._currentStep == 2 &&
                                              isChecked == false) {
                                            setState(() {
                                              registerColor =
                                                  Colors.teal.shade100;
                                            });
                                          }

                                          //if voor bovenste buttons van stepper
                                          if (this._currentStep == 0) {
                                            setState(() {
                                              stateButton1 = StepState.editing;
                                              stateButton2 = StepState.indexed;
                                              if (stateButton3 !=
                                                  StepState.disabled) {
                                                stateButton3 =
                                                    StepState.indexed;
                                              } else {
                                                stateButton3 =
                                                    StepState.disabled;
                                              }
                                            });
                                          } else if (this._currentStep == 1) {
                                            setState(() {
                                              stateButton2 = StepState.editing;
                                              stateButton1 = StepState.indexed;
                                              if (stateButton3 !=
                                                  StepState.disabled) {
                                                stateButton3 =
                                                    StepState.indexed;
                                              } else {
                                                stateButton3 =
                                                    StepState.disabled;
                                              }
                                            });
                                          } else {
                                            setState(() {
                                              stateButton3 = StepState.editing;
                                              stateButton2 = StepState.indexed;
                                              stateButton1 = StepState.indexed;
                                            });
                                          }
                                        },
                                        onStepContinue: () {
                                          if (this._currentStep <
                                              this._mySteps().length - 1) {
                                            if (this._currentStep == 0) {
                                              if (formkey.currentState!
                                                  .validate()) {
                                                setState(
                                                  () {
                                                    this._currentStep =
                                                        this._currentStep + 1;
                                                    this.stateButton2 =
                                                        StepState.editing;
                                                    this.stateButton1 =
                                                        StepState.indexed;
                                                  },
                                                );
                                              }
                                            } else if (this._currentStep == 1) {
                                              //als die 1 is en er dan opgedrukt wordt
                                              if (formkey.currentState!
                                                  .validate()) {
                                                setState(
                                                  () {
                                                    this._currentStep =
                                                        this._currentStep + 1;
                                                    this.stateButton2 =
                                                        StepState.indexed;
                                                    this.stateButton3 =
                                                        StepState.editing;
                                                  },
                                                );
                                              }
                                            } else if (this._currentStep ==
                                                2) {}
                                          }
                                          //als geaccpteerd en gevalideerd dan pas registreren
                                          else {
                                            if (isChecked == true &&
                                                formkey.currentState!
                                                    .validate()) {
                                              setState(
                                                () {
                                                  loading = !loading;
                                                  _registerUser();
                                                },
                                              );
                                            }
                                          }

                                          if (this._currentStep == 2 &&
                                              isChecked == false) {
                                            setState(() {
                                              registerColor =
                                                  Colors.teal.shade100;
                                              return null;
                                            });
                                          }

                                          //als er op een willekeurige knop wordt gedrukt
                                          if (this._currentStep == 2) {
                                            setState(() {
                                              this.btnText = 'Registreer';
                                            });
                                          }
                                        },
                                        onStepCancel: () {
                                          formkey.currentState!.reset();

                                          setState(() {
                                            if (this._currentStep > 0) {
                                              this._currentStep =
                                                  this._currentStep - 1;
                                            } else {
                                              this._currentStep = 0;
                                            }
                                          });

                                          if (this._currentStep == 1) {
                                            setState(() {
                                              this.stateButton2 =
                                                  StepState.editing;
                                              this.stateButton1 =
                                                  StepState.indexed;
                                              this.stateButton3 =
                                                  StepState.indexed;
                                            });
                                          } else if (this._currentStep == 2) {
                                            setState(() {
                                              this.stateButton3 =
                                                  StepState.editing;
                                              this.stateButton1 =
                                                  StepState.indexed;
                                              this.stateButton2 =
                                                  StepState.indexed;
                                            });
                                          } else {
                                            setState(() {
                                              this.stateButton1 =
                                                  StepState.editing;
                                              this.stateButton2 =
                                                  StepState.indexed;
                                              this.stateButton3 =
                                                  StepState.indexed;
                                            });
                                          }

                                          if (this._currentStep == 0 ||
                                              this._currentStep == 1) {
                                            setState(() {
                                              registerColor = Color(0xff009995);
                                              this.btnText = 'Volgende stap';
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  )),
                            ),
                            SizedBox(
                              height: 15,
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      )), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  List<Step> _mySteps() {
    List<Step> _steps = [
      Step(
          title: Text('Account'),
          state: stateButton1,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      validator: (val) => val!.isEmpty
                          ? 'Email adres dient ingevoert te worden'
                          : null,
                      decoration: kInputDecoration('Email *', Icon(Icons.mail)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      validator: (val) => val!.length < 6
                          ? 'Minimaal 6 karakters vereist'
                          : null,
                      decoration:
                          kInputDecoration('Wachtwoord *', Icon(Icons.lock)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: passwordConfirmController,
                      obscureText: true,
                      validator: (val) => val != passwordController.text
                          ? 'wachtwoorden matchen niet'
                          : null,
                      decoration: kInputDecoration(
                          'Herhaal wachtwoord *', Icon(Icons.lock)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
          isActive: _currentStep >= 0),
      Step(
          state: stateButton2,
          title: Text('Persoonsgegevens'),
          content: Container(
            margin: const EdgeInsets.only(top: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: firstNameController,
                  keyboardType: TextInputType.name,
                  validator: (val) => val!.isEmpty ? 'Vul voornaam in' : null,
                  decoration:
                      kInputDecoration('Voornaam *', Icon(Icons.person)),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: middleNameController,
                  keyboardType: TextInputType.name,
                  decoration:
                      kInputDecoration('Tussenvoegsel', Icon(Icons.person)),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: lastNameController,
                  keyboardType: TextInputType.name,
                  validator: (val) => val!.isEmpty ? 'Vul achternaam in' : null,
                  decoration:
                      kInputDecoration('Achternaam *', Icon(Icons.person)),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _textEditingController,
                  keyboardType: TextInputType.number,
                  decoration: kInputDecoration(
                      'Geboortedatum *', Icon(Icons.date_range)),
                  validator: (val) =>
                      val!.isEmpty ? 'Voer geboortedatum in' : null,
                  onTap: () => _selectDate(context),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          isActive: _currentStep >= 1),
      Step(
          title: Text('Musici gegevens'),
          state: stateButton3,
          content: Container(
            margin: const EdgeInsets.only(top: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: DropdownSearch(
                    dropdownSearchDecoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.only(
                            left: 10, top: 0, bottom: 0, right: 0)),
                    items: ["Man", "Vrouw", "Anders"],
                    label: "Geslacht *",
                    onChanged: (newValue) {
                      setState(() {
                        _valueGender = newValue;
                      });
                    },
                    validator: (item) {
                      if (item == null) {
                        return "Voer geslacht in";
                      }
                    },
                    maxHeight: 200,
                    dialogMaxWidth: 200,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: emailDoctorController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: kInputDecoration('Email arts', Icon(Icons.email)),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: musicalInstrumentController,
                  keyboardType: TextInputType.name,
                  decoration: kInputDecoration(
                      'Muziekinstrument', Icon(Icons.music_note)),
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            ElevatedButton.icon(
                              icon: Icon(Icons.image),
                              label: Text('Profielfoto'),
                              onPressed: () {
                                chooseImage(ImageSource.gallery);
                              },
                            ),
                            imageFile != null
                                ? TextButton.icon(
                                    icon: Icon(Icons.person),
                                    label: Text('Bekijk profielfoto'),
                                    onPressed: () {
                                      showDialog<void>(
                                        context: context,
                                        barrierDismissible:
                                            true, // user must tap button!
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: text_h1('Bekijk profielfoto',
                                                Colors.black),
                                            content: SingleChildScrollView(
                                              child: Column(
                                                children: [
                                                  bodytext(
                                                      'Onderstaand is te zien hoe de gekozen profielfoto eruit komt te zien.',
                                                      Colors.black),
                                                  Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 60.0),
                                                          width: 200,
                                                          height: 200,
                                                          decoration:
                                                              BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  image: DecorationImage(
                                                                      image: FileImage(
                                                                          imageFile!),
                                                                      fit: BoxFit
                                                                          .fill),
                                                                  border: Border
                                                                      .all(
                                                                    color: Theme.of(
                                                                            context)
                                                                        .colorScheme
                                                                        .primaryVariant,
                                                                    width: 1.5,
                                                                  )))
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                            actions: <Widget>[
                                              ElevatedButton(
                                                  style: ElevatedButton
                                                      .styleFrom(
                                                          elevation: 8,
                                                          minimumSize: Size(
                                                              130, 35),
                                                          onPrimary: Colors
                                                              .white,
                                                          primary: Theme.of(
                                                                  context)
                                                              .colorScheme
                                                              .primaryVariant),
                                                  onPressed: Navigator.of(
                                                          context)
                                                      .pop,
                                                  child: Text(
                                                      'Sluit',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 15))),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  )
                                : Text('Geen foto geselecteerd'),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            bodytext('Accepteer dat uw data opgeslagen wordt',
                                Colors.black),
                            Checkbox(
                              value: isChecked,
                              checkColor: Colors.white,
                              activeColor:
                                  Theme.of(context).colorScheme.primaryVariant,
                              onChanged: (bool? value) {
                                setState(() {
                                  isChecked = value!;
                                });
                                if (isChecked == false) {
                                  setState(() {
                                    registerColor = Colors.teal.shade100;
                                  });
                                } else {
                                  registerColor = Theme.of(context)
                                      .colorScheme
                                      .primaryVariant;
                                }
                              },
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          isActive: _currentStep >= 2)
    ];
    return _steps;
  }

  // Widget imageProfile() {
  //   return Stack(children: [
  //     CircleAvatar(
  //         radius: 80.0, backgroundImage: AssetImage('assets/default.png'))
  //   ]);
  // }
}
