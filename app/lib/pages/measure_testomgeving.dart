import 'package:firstproject/pages/measuresucces.dart';
import 'package:firstproject/widgets/measurebox.dart';
import 'package:firstproject/widgets/measureboxTestomgeving.dart';
import 'package:firstproject/widgets/nav.dart';
import 'package:flutter/material.dart';

import 'measuresucces_testomgeving.dart';

class MeasurePageTestomgeving extends StatelessWidget {
  const MeasurePageTestomgeving({Key? key, required this.hand, required this.step, required this.handList})
      : super(key: key);

  final String hand;
  final int step;
  final List handList;

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () => showAlert(context));

    return Scaffold(
        drawer: const nav(),
        appBar: AppBar(
            foregroundColor: Color(0xff009995),
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            actions: <Widget>[
              Image.asset(
                'assets/logo_revalidatie.png',
                fit: BoxFit.contain,
                height: 60,
              ),
              Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
            ],
            elevation: 0,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  iconSize: 50,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            )),
        body: Stack(
          children: <Widget>[
            if (step == 5) ...[
              MeasureSuccessTestomgeving()
            ] else ...[
              MeasureBoxTestomgeving(
                  selectedHand: hand,
                  stepCounter: step,
                  handObjectList: handList)
            ]
          ],
        ));
  }

  void showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: Text("Let erop dat uw de hand volledig strekt bij het meten"),
        ));
  }
}
