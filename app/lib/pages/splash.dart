import 'package:firstproject/intro.dart';
import 'package:flutter/material.dart';

import '../login.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async {
    await Future.delayed(Duration(milliseconds: 3000), () {});
    // navigeer vervolgens van het splashscreen naar de homescreen
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Intro()));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Center(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Center(
                child: Image(
                    image: AssetImage('assets/logo_revalidatie.png'),
                    width: 0.8 * width)),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Powered by Hogeschool Leiden",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ))
        ],
      ),
    ));
  }
}
