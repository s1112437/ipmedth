import 'package:firstproject/models/api_response_data.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/widgets/headline2.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../primary_button.dart';

class MeasureSuccessTestomgeving extends StatefulWidget {
  const MeasureSuccessTestomgeving();

  @override
  _MeasureSuccessStateTestomgeving createState() =>
      _MeasureSuccessStateTestomgeving();
}

class _MeasureSuccessStateTestomgeving
    extends State<MeasureSuccessTestomgeving> {
  _MeasureSuccessStateTestomgeving(); //constructor

  @override
  void initState() {
    setTestTrue();
    super.initState();
  }

  Future<SharedPreferences?> setTestTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('testDone', true);

    return prefs;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Container(
          padding: EdgeInsets.only(left: 250, right: 250),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: MediaQuery.of(context).size.width * 0.1,
                backgroundColor: Color(0xff009995),
                child: Icon(
                  Icons.check,
                  size: 80,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              text_h2("Testmeting succesvol geslaagd!", Colors.black),
              SizedBox(
                height: 20,
              ),
              Text(
                "In de testomgeving heeft u geleerd hoe de verschillende metingen werken. Nu bent u gereed om de echte meting te beginnen!",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                // padding: EdgeInsets.only(left: 75, right: 75),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    PrimaryButton(
                        buttonText: "Start meting",
                        buttonFunction: "Terug naar start",
                        buttonHeight: 50,
                        buttonWidth: 200)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
    return Container(
        child: PrimaryButton(
            buttonText: "Terug naar start",
            buttonFunction: "Terug naar start",
            buttonHeight: 50,
            buttonWidth: 250));
  }
}
