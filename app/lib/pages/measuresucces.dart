import 'package:firstproject/models/api_response_data.dart';
import 'package:firstproject/primary_button.dart';
import 'package:firstproject/results.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/widgets/bodytext.dart';
import 'package:firstproject/widgets/headline2.dart';
import 'package:flutter/material.dart';

import '../secondary_button.dart';

class MeasureSuccess extends StatefulWidget {
  const MeasureSuccess(
      {Key? key, required this.selectedHand, required this.handObjectList})
      : super(key: key);

  final String selectedHand;
  final List handObjectList;

  @override
  _MeasureSuccessState createState() =>
      _MeasureSuccessState(selectedHand, handObjectList);
}

class _MeasureSuccessState extends State<MeasureSuccess> {
  _MeasureSuccessState(this.selectedHand, this.handObjectList); //constructor

  final selectedHand;
  var handObjectList;
  var metingId;
  bool loading = true;
  bool requestGet = false;

  void _createMeasurement() async {
    ApiResponse response =
        await createUserMeasurement(selectedHand, handObjectList);
    if (response.error == null) {
      metingId = response.data;
      setState(() {
        loading = false;
        requestGet = true;
      });
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    _createMeasurement();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Visibility(
        visible: (requestGet == true),
        child: Center(
          child: loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  padding: EdgeInsets.only(left: 250, right: 250),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: MediaQuery.of(context).size.width * 0.1,
                        backgroundColor: Color(0xff009995),
                        child: Icon(
                          Icons.check,
                          size: 80,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      text_h2("Meting succesvol geslaagd!", Colors.black),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "De meting tussen de verschillende vingertoppen is succesvol geslaagd. In deze app kunt u de metingen terugkijken via de navigatie of de onderstaande linkerknop. Wil je meer weten van de specifieke meting? Ga dan via de rechterknop naar de site!",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        // padding: EdgeInsets.only(left: 75, right: 75),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SecondaryButton(
                                buttonText: "Bekijk meting",
                                buttonFunction: "naarmeting$metingId",
                                buttonHeight: 50,
                                buttonWidth: 200),
                            SizedBox(
                              width: 20,
                            ),
                            PrimaryButton(
                                buttonText: "Website",
                                buttonFunction: "naar website",
                                buttonHeight: 50,
                                buttonWidth: 200)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
        ),
        // child: Column(
        //   children: [
        //     ElevatedButton(
        //       onPressed: () => {
        //         Navigator.pushNamed(context, Results.routeName,
        //             arguments: ScreenArguments(metingId))
        //       },
        //       child: Text("bekijk meting"),
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
