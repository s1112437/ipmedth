import 'package:flutter/material.dart';
import 'package:firstproject/secondary_button.dart';
import 'package:firstproject/primary_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'widgets/nav.dart';

class HandOpmeten extends StatefulWidget {
  const HandOpmeten({
    Key? key,
  }) : super(key: key);

  @override
  State<HandOpmeten> createState() => _HandOpmetenState();
}

class _HandOpmetenState extends State<HandOpmeten> {
  bool testDone = false;
  SharedPreferences? pref;

  Future<SharedPreferences?> getTestDone() async {
    pref = await SharedPreferences.getInstance();

    setState(() {
      testDone = pref!.getBool("testDone") ?? false;
    });
    return pref;
  }

  @override
  void initState() {
    getTestDone();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: nav(),
        appBar: AppBar(
            foregroundColor: Color(0xff009995),
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            actions: <Widget>[
              Image.asset(
                'assets/logo_revalidatie.png',
                fit: BoxFit.contain,
                height: 60,
              ),
              Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
            ],
            elevation: 0,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  iconSize: 50,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            )),
        body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  // margin: EdgeInsets.only(
                  //     top: MediaQuery.of(context).size.height * 0.2),
                  padding: EdgeInsets.all(40.0),
                  decoration: BoxDecoration(
                    color: const Color(0xFFF6F6F6),
                    border: Border.all(color: const Color(0xFF949494)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height * 0.6,
                      maxWidth: MediaQuery.of(context).size.width * 0.8),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 30),
                              child: Text(
                                "Hand opmeten",
                                style: Theme.of(context).textTheme.headline1,
                              ),
                            ),
                            SizedBox(
                                width: 800,
                                child: Text(
                                  "In de volgende stap gaat u 8 verschillende punten van uw hand opmeten. Dit zal ongeveer 2 minuten duren. Druk op volgende om te beginnen",
                                  style: Theme.of(context).textTheme.headline3,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 10,
                                )),
                            Container(
                                margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.1,
                                    bottom: 25),
                                child: Text(
                                  "Wilt u testen?",
                                  style: Theme.of(context).textTheme.headline3,
                                )),
                            Container(
                                child: SecondaryButton(
                                    buttonText: "Naar testomgeving",
                                    buttonFunction: "testomgeving",
                                    buttonHeight: 50,
                                    buttonWidth: 240))
                          ],
                        ),
                      ),
                      const Expanded(
                          flex: 3,
                          child: Image(
                            image: AssetImage('assets/hand_inscannen.png'),
                          ))
                    ],
                  )),
              Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.height * 0.15,
                      right: MediaQuery.of(context).size.height * 0.15,
                      top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Visibility(
                          visible: testDone,
                          child: PrimaryButton(
                              buttonText: "Volgende",
                              buttonFunction: "volgende hand select",
                              buttonHeight: 50,
                              buttonWidth: 150))
                    ],
                  ))
            ]));
  }
}
