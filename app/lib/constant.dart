import 'package:firstproject/Measurement.dart';
import 'package:flutter/material.dart';

//--API ROUTES ---
//runnen via aaron
// const storageURL = "http://localhost:5000/storage";
// const baseURL = "http://localhost:5000/api";

// const storageURL = "https://ipmedth-api.aaronkoopstra.nl/storage";
// const baseURL = "https://ipmedth-api.aaronkoopstra.nl/api";
//runnen via emulator
const storageURL = "http://10.0.2.2:5000/storage";
const baseURL = "http://10.0.2.2:5000/api";

const loginURL = baseURL + '/login/';
const registerURL = baseURL + '/register/';
const logoutURL = baseURL + '/logout';
const userURL = baseURL + '/user';
const measurementsURL = baseURL + '/measurements';
const measurementURL = baseURL + '/measurement';
const createMeasurementURL = baseURL + '/create-measurement';
const forgotPasswordURL = baseURL + '/forget-password';

// ----- Errors -----
const serverError = 'Server error';
const unauthorized = 'Unauthorized';
const somethingWentWrong = 'Something went wrong, try again!';
