import 'dart:async';
import 'dart:convert';

import 'package:firstproject/hand_opmeten.dart';
import 'package:firstproject/results.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firstproject/Measurement.dart';
import 'package:firstproject/results.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:firstproject/services/user_service.dart';

import 'constant.dart';
import 'intro.dart';
import 'widgets/nav.dart';

Future<List<Measurement>> getMeasurements() async {
  String token = await getToken();
  final response = await http.get(Uri.parse(measurementsURL),
      headers: {'Authorization': 'Bearer $token'});
  print(response.body);
  if (response.statusCode == 200) {
    var responseJson = json.decode(response.body);
    return (responseJson as List).map((m) => Measurement.fromJson(m)).toList();
  } else {
    throw Exception('Fout bij laden van metingen');
  }
}

Future<http.Response> deleteMeasurement(
    int measurementId, BuildContext context) async {
  String token = await getToken();
  final http.Response response = await http.delete(Uri.parse(measurementURL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(<String, int>{'measurementId': measurementId}));

  if (json.decode(response.body)['status'] == 1) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyMeasurements()));
  } else {
    showError(context);
  }

  return response;
}

class MyMeasurements extends StatefulWidget {
  const MyMeasurements({Key? key}) : super(key: key);

  @override
  MyMeasurementState createState() => MyMeasurementState();
}

class MyMeasurementState extends State<MyMeasurements> {
  late Future<List<Measurement>> futureMeasurement;

  @override
  void initState() {
    super.initState();
    futureMeasurement = getMeasurements();
  }

  void showError() async {
    await Future.delayed(Duration(microseconds: 1));
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
          title: Text("Fout bij laden metingen"),
          content:
              Text('Er is een fout opgetreden bij het ophalen van uw metingen'),
          actions: <Widget>[
            FlatButton(
              // FlatButton widget is used to make a text to work like a button
              textColor: Colors.red,
              onPressed: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Intro()));
              },
              child: const Text(
                  'SLUITEN'), // function used to perform after pressing the button
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: nav(),
        backgroundColor: Colors.white,
        appBar: AppBar(
            foregroundColor: Color(0xff009995),
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            actions: <Widget>[
              Image.asset(
                'assets/logo_revalidatie.png',
                fit: BoxFit.contain,
                height: 60,
              ),
              Container(margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
            ],
            elevation: 0,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  iconSize: 50,
                  color: Theme.of(context).colorScheme.primaryVariant,
                  tooltip:
                      MaterialLocalizations.of(context).openAppDrawerTooltip,
                );
              },
            )),
        body: Center(
          child: Container(
            // alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width * 0.7,
            // margin: const EdgeInsets.only(top: 40.0, bottom: 40.0),
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Color(0xFFF6F6F6),
              borderRadius: BorderRadius.circular(5),
              border: Border.all(width: 1.0, color: Color(0xFF949494)),
            ),
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    alignment: Alignment.topLeft,
                    width: MediaQuery.of(context).size.width * 0.45,
                    margin: const EdgeInsets.only(
                        bottom: 10.0, left: 10.0, right: 10.0),
                    child: Text('Mijn metingen',
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w600,
                            fontFamily: GoogleFonts.openSans().fontFamily))),
                Container(
                  width: MediaQuery.of(context).size.width * 0.325,
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "Metingen",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "Datum",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          )
                        ],
                      ),
                      Column(
                        children: [Container(width: 60)],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                    // height: MediaQuery.of(context).size.height * 0.6,
                    width: MediaQuery.of(context).size.width * 1,
                    child: new FutureBuilder<List<Measurement>>(
                        future: futureMeasurement,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            List<Measurement>? measurements = snapshot.data;
                            return GridView(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  mainAxisExtent: 76,
                                  mainAxisSpacing: 20,
                                ),
                                children: measurements!
                                    .map((measurement) => new InkWell(
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                right: 10.0, left: 10.0),
                                            padding: const EdgeInsets.all(10.0),
                                            width: 412,
                                            height: 76,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.stretch,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                new Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        '${measurement.hand} hand',
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        convertDate(measurement
                                                            .created_at),
                                                      ),
                                                      IconButton(
                                                          icon: const Icon(
                                                              Icons.delete,
                                                              color: Colors.red,
                                                              size: 24.0),
                                                          tooltip:
                                                              'Verwijder meting',
                                                          onPressed: () {
                                                            showDialog<String>(
                                                              context: context,
                                                              builder: (BuildContext
                                                                      context) =>
                                                                  AlertDialog(
                                                                title: const Text(
                                                                    "Meting verwijderen"),
                                                                content: const Text(
                                                                    "Weet u zeker dat u de meting wilt verwijderen?"),
                                                                actions: <
                                                                    Widget>[
                                                                  ElevatedButton(
                                                                      style: ElevatedButton.styleFrom(
                                                                          primary: Color(
                                                                              0xff009995)),
                                                                      onPressed: () => Navigator.pop(
                                                                          context,
                                                                          'Annuleren'),
                                                                      child: const Text(
                                                                          'Annuleren')),
                                                                  ElevatedButton(
                                                                      style: ElevatedButton.styleFrom(
                                                                          primary: Colors
                                                                              .red),
                                                                      onPressed:
                                                                          () =>
                                                                              {
                                                                                deleteMeasurement(measurement.id, context),
                                                                              },
                                                                      child: const Text(
                                                                          'Verwijderen')),
                                                                ],
                                                              ),
                                                            );
                                                          })
                                                    ])
                                              ],
                                            ),
                                            decoration: new BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.25),
                                                    offset: Offset(0, 4),
                                                    blurRadius: 4,
                                                  )
                                                ]),
                                          ),
                                          onTap: () {
                                            Navigator.pushNamed(
                                                context, Results.routeName,
                                                arguments: ScreenArguments(
                                                    measurement.id));
                                          },
                                          enableFeedback: true,
                                        ))
                                    .toList());
                          } else if (snapshot.hasError) {
                            showError();
                          }
                          return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularProgressIndicator(
                                    color: Color(0xFF00AAA6))
                              ]);
                        })),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: ElevatedButton(
                        child: const Text("Nieuwe meting starten",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            )),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return (HandOpmeten());
                          }));
                        },
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xFF00AAA6)),
                            minimumSize:
                                MaterialStateProperty.all<Size>(Size(200, 50))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }

  String convertDate(String dateIn) {
    DateTime parseDate =
        new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(dateIn);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    var outputDate = outputFormat.format(inputDate);
    return outputDate;
  }
}
