import 'package:flutter/material.dart';

Text text_h1(String label, Color color) {
  return Text(
    label,
    style: TextStyle(color: color, fontSize: 35, fontWeight: FontWeight.bold),
  );
}
