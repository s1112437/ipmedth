import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class AnimationWidget extends StatefulWidget {
  final String animationName;

  AnimationWidget(this.animationName);

  @override
  _AnimationWidgetState createState() => _AnimationWidgetState(animationName);
}

class _AnimationWidgetState extends State<AnimationWidget> {
  late RiveAnimationController _controller;
  String animationName;
  String assetName = "";

  bool _isPlaying = true;
  bool _visible = true;

  _AnimationWidgetState(this.animationName);

  void _togglePlay() =>
      setState(() => _controller.isActive = !_controller.isActive);

  bool get isPlaying => _controller.isActive;

  @override
  void initState() {
    super.initState();
    setState(() {
      assetName = "assets/" + animationName + ".riv";
    });
    _controller = OneShotAnimation('idle');
  }

  @override
  Column build(BuildContext context) {
    return Column(children: [
      Container(
        height: 150,
        width: 150,
        child: RiveAnimation.asset(
          "assets/" + animationName + ".riv",
          alignment: Alignment.topCenter,
          // controllers: [_controller],
          // Update the play state when the widget's initialized
          onInit: (_) => setState(() {}),
        ),
      ),
    ]);
  }
}
