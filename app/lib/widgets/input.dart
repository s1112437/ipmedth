// --- input decoration
import 'package:flutter/material.dart';

var iconen = [Icons.email, Icons.password];
InputDecoration kInputDecoration(String label, Icon? icoon) {
  return InputDecoration(
      prefixIcon: icoon,
      labelText: label,
      filled: true,
      fillColor: Colors.white,
      contentPadding: EdgeInsets.all(10),
      border: OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: Colors.black)));
}
