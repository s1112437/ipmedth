import 'package:firstproject/models/api_response_data.dart';
import 'package:firstproject/models/user.dart';
import 'package:firstproject/services/user_service.dart';
import 'package:firstproject/widgets/bodytext.dart';
import 'package:firstproject/widgets/headline1.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constant.dart';
import '../hand_opmeten.dart';
import '../hand_opmeten_testomgeving.dart';
import '../myMeasurements.dart';
import '../register.dart';
import 'headline2.dart';

class nav extends StatefulWidget {
  const nav({Key? key}) : super(key: key);

  @override
  State<nav> createState() => navState();
}

class navState extends State<nav> {
  bool loading = false;
  SharedPreferences? pref;
  String? firstname;
  String? middlename;
  String? lastname;
  String? musicalInstrument;
  String? dateOfBirth;
  String? email;
  String? imageFile;
  String? imagePath = '';
  int? age;

  int isAdult(String birthDateString) {
    String datePattern = "yyyy-MM-dd";

    DateTime birthDate = DateFormat(datePattern).parse(birthDateString);
    DateTime today = DateTime.now();

    int yearDiff = today.year - birthDate.year;
    int monthDiff = today.month - birthDate.month;
    int dayDiff = today.day - birthDate.day;
    if (monthDiff < 0) {
      if (dayDiff < 0) {
        yearDiff = yearDiff - 1;
      }
    }

    setState(() {
      this.age = yearDiff;
      loading = false;
    });
    return yearDiff;
  }

  Future<SharedPreferences?> getUser() async {
    setState(() {
      loading = true;
    });
    pref = await SharedPreferences.getInstance();

    setState(() {
      this.firstname = pref!.getString("firstname");
      this.middlename = pref!.getString("middlename");
      this.lastname = pref!.getString("lastname");
      this.musicalInstrument = pref!.getString("musicalInstrument");
      this.dateOfBirth = pref!.getString("dateOfBirth");
      this.email = pref!.getString("email");
      if (pref!.getString("image") == '') {
        this.imageFile = pref!.getString("image");
        this.imagePath = 'https://i.imgur.com/jNNT4LE.jpg';
      } else {
        this.imageFile = pref!.getString("image");
        this.imagePath = storageURL + '/profiles/' + imageFile!;
      }
    });
    isAdult(dateOfBirth!);
    print(pref!.getString("image"));
    print(imagePath);
    return pref;
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final padding = EdgeInsets.symmetric(horizontal: 20);
    return Drawer(
      child: Material(
          color: Theme.of(context).colorScheme.primaryVariant,
          child: ListView(
            padding: padding,
            children: [
              Center(
                  child: loading
                      ? Center(
                          child: CircularProgressIndicator(color: Colors.white),
                        )
                      : Container(
                          margin: const EdgeInsets.only(top: 60.0),
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage(imagePath!),
                                  fit: BoxFit.fill)))),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if ('$middlename' == '') ...[
                      text_h1('$firstname $lastname', Colors.white)
                    ] else ...[
                      text_h1('$firstname $middlename $lastname', Colors.white),
                    ],
                    SizedBox(
                      height: 10,
                    ),
                    // text_h2("Gegevens:", Colors.white),
                    if ('$musicalInstrument' == '')
                      ...[]
                    else ...[
                      bodytext("Bespeelt $musicalInstrument", Colors.white),
                    ],
                    bodytext("Is $age jaar oud", Colors.white),
                    bodytext("Email: $email", Colors.white)
                  ],
                ),
              ),
              SizedBox(
                height: 60,
              ),
              buildMenuItem(
                  text: 'Testomgeving',
                  icon: Icons.book,
                  onClicked: () => selectedItem(context, 0)),
              Divider(
                color: Colors.white,
                height: 10,
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
              SizedBox(
                height: 5,
              ),
              buildMenuItem(
                  text: 'Mijn metingen',
                  icon: Icons.rule_rounded,
                  onClicked: () => selectedItem(context, 1)),
              Divider(
                color: Colors.white,
                height: 10,
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
              SizedBox(
                height: 5,
              ),
              buildMenuItem(
                  text: 'Bezoek website',
                  icon: Icons.web,
                  onClicked: () => selectedItem(context, 3)),
              Divider(
                color: Colors.white,
                height: 10,
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
              SizedBox(
                height: 5,
              ),
              buildMenuItem(
                  text: 'Start meting',
                  icon: Icons.height,
                  onClicked: () => selectedItem(context, 4)),
              Divider(
                color: Colors.white,
                height: 10,
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
              SizedBox(
                height: 5,
              ),
              buildMenuItem(
                  text: 'Log uit',
                  icon: Icons.logout,
                  onClicked: () => selectedItem(context, 2)),
              Divider(
                color: Colors.white,
                height: 10,
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
            ],
          )),
    );
  }

  void selectedItem(BuildContext context, int index) {
    print("in functie");
    switch (index) {
      case 0:
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HandOpmetenTestomgeving()),
            (route) => false);
        break;
      case 1:
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => MyMeasurements()),
            (route) => false);
        break;
      case 2:
        logout().then((value) => {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) =>
                          RegisterPage(title: "Revalidatie Friesland")),
                  (route) => false)
            });
        break;
      case 3:
        var url = 'https://ipmedth-groep4-web.herokuapp.com/';
        launch(url);
        break;
      case 4:
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => HandOpmeten()),
            (route) => false);
        break;
      default:
    }
  }
}

Widget buildMenuItem(
    {required String text, required IconData icon, VoidCallback? onClicked}) {
  final color = Colors.white;

  return ListTile(
    leading: Icon(icon, color: color),
    onTap: onClicked,
    title: Text(
      text,
      style: TextStyle(color: color, fontWeight: FontWeight.bold),
    ),
  );
}
