import 'package:flutter/material.dart';

Text text_h2(String label, Color color) {
  return Text(
    label,
    style: TextStyle(color: color, fontSize: 25, fontWeight: FontWeight.bold),
  );
}
