import 'dart:math';
import 'dart:ui';

import 'package:firstproject/pages/measure.dart';
import 'package:firstproject/pages/measure_testomgeving.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:rive/rive.dart';

import 'headline1.dart';
import 'headline2.dart';

class MeasureBoxTestomgeving extends StatefulWidget {
  const MeasureBoxTestomgeving(
      {Key? key,
      required this.selectedHand,
      required this.stepCounter,
      required this.handObjectList})
      : super(key: key);

  final String selectedHand;
  final int stepCounter;
  final List handObjectList;

  @override
  MeasureBoxStateTestomgevingState createState() =>
      MeasureBoxStateTestomgevingState(
          selectedHand, this.stepCounter, this.handObjectList);
}

class MeasureBoxStateTestomgevingState extends State<MeasureBoxTestomgeving> {
  MeasureBoxStateTestomgevingState(
      this.selectedHand, this.stepCounter, this.handObjectList); //constructor

  // var test = Hand("test", "something", 0.0);
  var handObjectList;
  var selectedHand;
  var stepCounter;

  var animationString = "assets/span_1-2.riv";

  List<TouchPoints> points = [];

  // List<double> measurements = [];

  var measurements = [];

  double _distance = 0.0;
  int maxSteps = 5;
  bool _measureComplete = false;
  bool _allMeasurmentsComplete = false;

  String textHeader =
      "Leg steeds de twee fingertoppen neer die worden aangegeven";

  double opacity = 1.0;
  StrokeCap strokeType = StrokeCap.round;
  double strokeWidth = 3.0;
  Color selectedColor = Colors.black;

  void _measureDistance() {
    // var DPI = 220;
    // var test = size.width / mediaQuery.devicePixelRatio;
    // var test3 = size.height / size.height;
    // var test2 = test * 160;
    if (points.length == 2) {
      var point1 = points[0].points;
      var point2 = points[1].points;
      //Todo: hardcoded value for now
      var dpi = 150;

      double xDistMin = (point1.dx - point2.dx).abs();
      double xDistDiv = xDistMin / dpi;
      num xDistPow = pow(xDistDiv, 2.0);

      double yDistMin = (point1.dy - point2.dy).abs();
      double yDistDiv = yDistMin / dpi;
      num yDistPow = pow(yDistDiv, 2.0);
      var distance = sqrt(xDistPow + yDistPow) * 2.54;
      handObjectList[stepCounter].distance = distance;
      setState(() {
        _distance = distance;
        _measureComplete = true;
      });
    }
  }

  void setStep(bool forward) {
    setState(() {
      if (forward) {
        stepCounter = stepCounter + 1;
        measurements.add(_distance);
        // measurements.insert(measurements.length, _distance);
        // measurements.add(_distance);
        // print(_distance);
      }
      //Check if value is step forward or backwards
      _distance = 0;
      _measureComplete = false;
      points = [];
      Paint();
    });

    if (stepCounter == maxSteps) {
      setState(() {
        _allMeasurmentsComplete = true;
        if (selectedHand == "rechts") {
          textHeader =
              "De metingen zijn gelukt voor de rechter hand, ga terug om het nu voor het echt te meten";
        } else {
          textHeader =
              "De metingen zijn gelukt voor de linker hand, ga terug om het nu voor het echt te meten";
        }
        points = [];
        Paint();
        return;
      });
    }
  }

  late AnimationController controller;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Listener(
        onPointerDown: (details) {
          if (!_measureComplete && !_allMeasurmentsComplete) {
            setState(() {
              RenderBox renderBox = context.findRenderObject() as RenderBox;
              points.add(TouchPoints(
                  points: renderBox.globalToLocal(details.position),
                  size: 15.0,
                  paint: Paint()
                    ..strokeCap = strokeType
                    ..isAntiAlias = true
                    ..color = const Color(0xFF00AAA6)
                    ..strokeWidth = strokeWidth));
              _measureDistance();
            });
          }
        },
        onPointerUp: (details) {
          if (!_measureComplete) {
            setState(() {
              //points = [];
              Paint();
            });
          }
        },
        child: Stack(
          children: [
            Container(
              padding: const EdgeInsets.all(15.0),
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(0.0),
                    child: CustomPaint(
                      size: Size.infinite,
                      painter: MyPainter(
                        callback: (val) => val,
                        pointsList: points,
                      ),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 200),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              text_h1(
                                  "Hand opmeten (testomgeving)", Colors.black),
                              text_h2(
                                  "Leg steeds de twee vingertoppen neer die worden aangegeven in de animatie:",
                                  Colors.black),
                              text_h2(
                                  "Afstand: " +
                                      (handObjectList[widget.stepCounter]
                                              .distance)
                                          .ceil()
                                          .toString() +
                                      "cm",
                                  Colors.black),
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                        child: LinearPercentIndicator(
                          animation: true,
                          lineHeight: 20.0,
                          animationDuration: 100,
                          percent: widget.stepCounter / maxSteps,
                          center: Text(
                            "${(widget.stepCounter / maxSteps * 100).ceil()}%",
                            style: TextStyle(color: Colors.white),
                          ),
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          progressColor: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5, right: 5),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.52,
                        alignment: Alignment.topRight,
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01),
                        decoration: BoxDecoration(
                          color: _measureComplete
                              ? const Color(0xFFE2FFDB)
                              : const Color(0xFFF6F6F6),
                          border: Border.all(
                              color: _measureComplete
                                  ? const Color(0xFF27B14E)
                                  : const Color(0xFF949494)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            CustomPaint(
              size: Size.infinite,
              painter: MyPainter(
                callback: (val) => val,
                pointsList: points,
              ),
            ),
            Container(
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.65,
                    left: MediaQuery.of(context).size.width * 0.8),
                child: Container(
                  height: 135,
                  width: 250,
                  child: RiveAnimation.asset(
                    "assets/" +
                        handObjectList[widget.stepCounter].animation +
                        ".riv",
                    alignment: Alignment.center,
                  ),
                )),
            Visibility(
                visible: _measureComplete && !_allMeasurmentsComplete,
                child: Container(
                    padding: EdgeInsets.only(
                        left: 15, right: 15, top: size.height * 0.79),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 150,
                            height: 50,
                            child: OutlinedButton(
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.all(10),
                                  primary: Colors.white,
                                  elevation: 5,
                                  side: const BorderSide(
                                      color: Color(0xFF00AAA6), width: 2),
                                  textStyle: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 24,
                                  )),
                              onPressed: () {
                                setStep(false);
                              },
                              child: Text('Opnieuw'),
                            )),
                        SizedBox(
                          width: 150,
                          height: 50,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(10),
                                primary: Color(0xFF00AAA6),
                                elevation: 5,
                                side: const BorderSide(
                                    color: Color(0xFF00AAA6), width: 2),
                                textStyle: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 24,
                                )),
                            onPressed: () {
                              setStep(true);
                              // animationName = animations[stepCounter];
                              if (stepCounter < maxSteps) {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return (MeasurePageTestomgeving(
                                      hand: 'rechts',
                                      step: stepCounter,
                                      handList: handObjectList));
                                }));
                              } else {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return (MeasurePageTestomgeving(
                                      hand: 'rechts',
                                      step: stepCounter,
                                      handList: handObjectList));
                                }));
                              }
                            },
                            child: Text('Verder'),
                          ),
                        ),
                      ],
                    ))),
          ],
        ),
      ),
    );
  }
}

typedef StringCallback = void Function(String val);

//Class to define a point touched at canvas
class TouchPoints {
  Paint paint;
  Offset points;
  double size;

  TouchPoints({required this.points, required this.paint, required this.size});
}

class MyPainter extends CustomPainter {
  final StringCallback callback;

  MyPainter({required this.callback, required this.pointsList});

  List<TouchPoints> pointsList;
  List<Offset> offsetPoints = [];

  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length; i++) {
      canvas.drawCircle(
          pointsList[i].points, pointsList[i].size, pointsList[i].paint);
    }
    if (pointsList.length == 2) {
      canvas.drawLine(
          pointsList[0].points, pointsList[1].points, pointsList[0].paint);
    }
  }

  @override
  bool shouldRepaint(MyPainter delegate) {
    return true;
  }
}
