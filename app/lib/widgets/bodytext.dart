import 'package:flutter/material.dart';

Text bodytext(String label, Color color) {
  return Text(
    label,
    style: TextStyle(color: color, fontSize: 15, fontWeight: FontWeight.normal),
  );
}
