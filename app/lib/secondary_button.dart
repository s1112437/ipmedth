import 'package:firstproject/hand_opmeten.dart';
import 'package:firstproject/hand_opmeten_testomgeving.dart';
import 'package:firstproject/results.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'myMeasurements.dart';

class SecondaryButton extends StatelessWidget {
  SecondaryButton(
      {required this.buttonText,
      required this.buttonFunction,
      required this.buttonHeight,
      required this.buttonWidth});

  String buttonText;
  String buttonFunction;
  double buttonHeight;
  double buttonWidth;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: buttonWidth,
        height: buttonHeight,
        child: OutlinedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(10),
              primary: Colors.white,
              elevation: 5,
              side: const BorderSide(color: Color(0xFF00AAA6), width: 2),
              textStyle: const TextStyle(
                color: Colors.black,
                fontSize: 24,
              )),
          onPressed: () {
            buttonHandler(buttonFunction, context);
          },
          child: Text(buttonText),
        ));
  }

  void buttonHandler(String action, BuildContext context) {
    if (action == "terug") {
      Navigator.pop(context, true);
    } else if (action == "testomgeving") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (HandOpmetenTestomgeving());
      }));
    } else if (action == "mijn gegevens") {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return (MyMeasurements());
      }));
    } else if (action.substring(0, 10) == "naarmeting") {
      try {
        final intValue = int.parse(action.replaceAll(RegExp('[^0-9]'), ''));
        Navigator.pushNamed(context, Results.routeName,
            arguments: ScreenArguments(intValue));
      } on FormatException {
        print('Format error!');
      }
    }
  }
}
