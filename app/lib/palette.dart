//palette.dart
import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor kToDark = const MaterialColor(
    0xff009995, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xff009995), //10%
      100: const Color(0xff008885), //20%
      200: const Color(0xff007774), //30%
      300: const Color(0xff006664), //40%
      400: const Color(0xff005553), //50%
      500: const Color(0xff004442), //60%
      600: const Color(0xff003332), //70%
      700: const Color(0xff002221), //80%
      800: const Color(0xff001111), //90%
      900: const Color(0xff000000), //100%
    },
  );
}
