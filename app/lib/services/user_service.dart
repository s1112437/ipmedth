import 'dart:convert';
import 'dart:io';

import 'package:firstproject/models/api_response_data.dart';
import 'package:firstproject/models/user.dart';
import 'package:firstproject/models/measurement_id.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';

//login function
Future<ApiResponse> loginUser(String email, String password) async {
  ApiResponse apiResponse = ApiResponse();
  try {
    final response = await http.post(Uri.parse(loginURL),
        headers: {'Accept': 'application/json'},
        body: {'email': email, 'password': password});

    switch (response.statusCode) {
      case 200:
        apiResponse.data = User.fromJson(jsonDecode(response.body));
        break;
      case 422:
        final errors = jsonDecode(response.body)['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      case 403:
        apiResponse.error = jsonDecode(response.body)['message'];
        break;
      default:
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    print(email);
    print(password);
    print(e);
    apiResponse.error = serverError;
  }

  return apiResponse;
}

//register function
Future<ApiResponse> registerUser(
    String email,
    String password,
    String firstname,
    String? middlename,
    String lastname,
    String sex,
    String? musicalInstrument,
    DateTime dateOfBirth,
    String? emailDoctor,
    String? image) async {
  ApiResponse apiResponse = ApiResponse();

  try {
    final response = await http.post(Uri.parse(registerURL), headers: {
      'Accept': 'application/json'
    }, body: {
      'firstname': firstname,
      'middlename': middlename,
      'lastname': lastname,
      'sex': sex,
      'musical_instrument': musicalInstrument,
      'date_of_birth': dateOfBirth.toString(),
      'email': email,
      'email_doctor': emailDoctor,
      'avatar_image': image,
      'password': password,
      'password_confirmation': password
    });

    print(response.body);

    switch (response.statusCode) {
      case 200:
        apiResponse.data = User.fromJson(jsonDecode(response.body));
        break;
      case 422:
        final errors = jsonDecode(response.body)['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      default:
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    print(e);
    apiResponse.error = serverError;
  }
  return apiResponse;
}

//register function
Future<ApiResponse> createUserMeasurement(String hand, List handList) async {
  ApiResponse apiResponse = ApiResponse();

  try {
    var chosenMap = {'hand': hand};
    var handMap = {
      for (var e in handList)
        e.animation.replaceAll("-", "_"): e.distance.toString()
    };

    var finalMap = {
      ...chosenMap,
      ...handMap,
    };

    debugPrint(finalMap.toString());

    String token = await getToken();
    final response = await http.post(
      Uri.parse(createMeasurementURL),
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
      body: finalMap,
    );
    Measurement.fromJson(jsonDecode(response.body));

    switch (response.statusCode) {
      case 200:
        apiResponse.data = json.decode(response.body)['measurement']['id'];
        break;
      case 422:
        final errors = jsonDecode(response.body)['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      default:
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    print(e);
    apiResponse.error = serverError;
  }
  return apiResponse;
}

//function user details
Future<ApiResponse> getUserDetail() async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    print(token);
    final response = await http.get(Uri.parse(userURL), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    });
    print(response.body.toString());
    switch (response.statusCode) {
      case 200:
        apiResponse.data = User.fromJson(jsonDecode(response.body));
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}

Future<ApiResponse> requestPassword(String Email) async {
  ApiResponse apiResponse = ApiResponse();

  try {
    final response = await http.post(Uri.parse(forgotPasswordURL), headers: {
      'Accept': 'application/json'
    }, body: {
      'email': Email,
    });
    print(response.statusCode);
    print(response.body);
    switch (response.statusCode) {
      case 200:
        break;
      case 302:
        break;
      case 422:
        final errors = jsonDecode(response.body)['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      default:
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    print(e);
    apiResponse.error = serverError;
  }

  // print(apiResponse.data);
  return apiResponse;
}

// get token
Future<String> getToken() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  return pref.getString('token') ?? '';
}

// get user id
Future<int> getUserId() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  return pref.getInt('userId') ?? 0;
}

// logout
Future<bool> logout() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  return await pref.remove('token');
}

String? getStringImage(File? file) {
  if (file == null) return null;
  return base64Encode(file.readAsBytesSync());
}
