class Hand {
  String animation;
  String span;
  double? distance;

  Hand(this.animation, this.span, this.distance);
}