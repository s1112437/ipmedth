import 'package:flutter/cupertino.dart';
import 'package:test/test.dart';
import 'package:firstproject/widgets/nav.dart';

void main() {
  test('test of de juiste leeftijd wordt teruggegeven', () {
    String birthDateString = '2001-03-18';
    const widget = nav();
    final element = widget.createElement();
    final state = element.state as navState;

    expect(state.isAdult(birthDateString), 20);
  });
}
