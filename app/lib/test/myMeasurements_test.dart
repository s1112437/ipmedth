import 'package:flutter/cupertino.dart';
import 'package:test/test.dart';
import 'package:firstproject/myMeasurements.dart';

void main() {
  test('test of de juiste datum wordt teruggegeven', () {
    const widget = MyMeasurements();
    final element = widget.createElement();
    final state = element.state as MyMeasurementState;

    expect(state.convertDate('2022-03-18T12:08:56.52'), '18-03-2022');
  });
}
