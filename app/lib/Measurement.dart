class Measurement {
  final int id;
  final int user_id;
  final String hand;
  final num span_1_2;
  final num span_1_3;
  final num span_1_4;
  final num span_1_5;
  final num span_2_3;
  final num span_2_4;
  final num span_2_5;
  final num span_3_4;
  final num span_3_5;
  final num span_4_5;
  // final num height_difference_1_3;
  // final num height_difference_3_5;
  // final num hand_width;
  // final num hand_length;
  final String created_at;

  Measurement({
    required this.id,
    required this.user_id,
    required this.hand,
    required this.span_1_2,
    required this.span_1_3,
    required this.span_1_4,
    required this.span_1_5,
    required this.span_2_3,
    required this.span_2_4,
    required this.span_2_5,
    required this.span_3_4,
    required this.span_3_5,
    required this.span_4_5,
    // required this.height_difference_1_3,
    // required this.height_difference_3_5,
    // required this.hand_width,
    // required this.hand_length,
    required this.created_at,
  });

  factory Measurement.fromJson(Map<String, dynamic> json) {
    return Measurement(
      id: json['id'],
      user_id: json['user_id'],
      hand: json['hand'],
      span_1_2: json['span_1_2'],
      span_1_3: json['span_1_3'],
      span_1_4: json['span_1_4'],
      span_1_5: json['span_1_5'],
      span_2_3: json['span_2_3'],
      span_2_4: json['span_2_4'],
      span_2_5: json['span_2_5'],
      span_3_4: json['span_3_4'],
      span_3_5: json['span_3_5'],
      span_4_5: json['span_4_5'],
      // height_difference_1_3: json['height_difference_1_3'],
      // height_difference_3_5: json['height_difference_3_5'],
      // hand_width: json['hand_width'],
      // hand_length: json['hand_length'],
      created_at: json['created_at'],
    );
  }
}
