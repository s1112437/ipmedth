import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

import 'login.dart';

class Intro extends StatefulWidget {
  const Intro({Key? key}) : super(key: key);

  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  List<Slide> slides = [];

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Registreer en login",
        styleTitle: TextStyle(
            color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),
        styleDescription: TextStyle(color: Colors.white, fontSize: 20),
        heightImage: 300,
        widthImage: 300,
        description:
            "Om gebruik te maken van deze app dien je, je te registreren. Bij het registreren vul je de persoonsgegevens in. Heb je een revalidatie arts? vul dan in het laatste veld deze gegevens in!",
        marginDescription: EdgeInsets.only(left: 200, right: 200, top: 100),
        pathImage: "assets/registreer.png",
        backgroundColor: Color(0xff009995),
      ),
    );
    slides.add(
      new Slide(
        title: "Meet de hand",
        styleTitle: TextStyle(
            color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),
        styleDescription: TextStyle(color: Colors.white, fontSize: 20),
        heightImage: 300,
        widthImage: 300,
        description:
            "Heb je last van een bepaalde hand, dan kan deze hand gemeten worden. Er worden verschillende hand posities doormiddel van een animatie weergegeven. Vooraf testen hoe het meten werkt is mogelijk in de testomgeving.",
        marginDescription: EdgeInsets.only(left: 200, right: 200, top: 100),
        pathImage: "assets/hand.png",
        backgroundColor: Color(0xff009995),
      ),
    );
    slides.add(
      new Slide(
        title: "Bekijk metingen",
        styleTitle: TextStyle(
            color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),
        styleDescription: TextStyle(color: Colors.white, fontSize: 20),
        heightImage: 300,
        widthImage: 300,
        description:
            "Nadat de hand gemeten is, zijn de resultaten van de verschillende handposities te zien. Wil je meer informatie over de hand? Ga dan naar de website met een druk op de knop. Op deze website zie je of je lengtes erg afwijken van het gemiddelde.",
        marginDescription: EdgeInsets.only(left: 200, right: 200, top: 100),
        pathImage: "assets/metingen.png",
        backgroundColor: Color(0xff009995),
      ),
    );
    slides.add(
      new Slide(
        title: "Gebruik van de data",
        styleTitle: TextStyle(
            color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),
        styleDescription: TextStyle(color: Colors.white, fontSize: 20),
        heightImage: 300,
        widthImage: 300,
        description:
            "De data van de hand wordt opgeslagen en gebruikt binnen revalidatie Friesland. Verder is uw data veilig, en zal deze niet over het internet verspreid worden.",
        marginDescription: EdgeInsets.only(left: 200, right: 200, top: 100),
        pathImage: "assets/data.png",
        backgroundColor: Color(0xff009995),
      ),
    );
  }

  Widget renderPreviousBtn() {
    return Icon(
      Icons.arrow_back,
      color: Color(0xffffffff),
      size: 35.0,
    );
  }

  ButtonStyle myButtonStyle() {
    return ButtonStyle(
      shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
      backgroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
    );
  }

  Widget renderSkipBtn() {
    return Icon(
      Icons.skip_next,
      color: Color(0xff009995),
    );
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Color(0xff009995),
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Color(0xff009995),
      size: 35.0,
    );
  }

  void onDonePress() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => login(title: "Revalidatie Friesland")),
        (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // //prev btn
      // renderPrevBtn: this.renderPreviousBtn(),
      // prevButtonStyle: ButtonStyle(
      //   shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
      //   backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFFFFFFF)),
      // ),

      //skip btn
      renderSkipBtn: this.renderSkipBtn(),
      skipButtonStyle: myButtonStyle(),

      //next btn
      renderNextBtn: this.renderNextBtn(),
      nextButtonStyle: myButtonStyle(),

      //done btn
      renderDoneBtn: this.renderDoneBtn(),
      onDonePress: this.onDonePress,
      doneButtonStyle: myButtonStyle(),

      colorActiveDot: Color(0xffffffff),
      sizeDot: 16.0,

      slides: this.slides,
    );
  }
}
