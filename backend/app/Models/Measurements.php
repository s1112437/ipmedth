<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Measurements extends Model
{
    use HasFactory;

    protected $table = 'measurements';

    protected $fillable = [
        'user_id',
        'hand',
        'span_1_2',
        'span_1_3',
        'span_1_4',
        'span_1_5',
        'span_2_3',
        'span_2_4',
        'span_2_5',
        'span_3_4',
        'span_3_5',
        'span_4_5',
        // 'height_difference_1_3',
        // 'height_difference_3_5',
        // 'hand_width',
        // 'hand_length',
        'email_doctor',
        'password',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

}
