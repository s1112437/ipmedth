<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Measurements;

class MeasurementController extends Controller
{
    public function index() {
        $userId = Auth::id();
        return User::find($userId)->measurements()->get();
    }

    public function show(Request $request) {
        $measurementId = $request->input('measurementId');
        return Measurements::find($measurementId);
    }

    public function delete(Request $request) {
        $measurementId = $request->input('measurementId');
        if(Measurements::find($measurementId)->delete()){
            $data=[
                'status' => 1,
                'msg' => 'success'
            ];
        } else {
            $data=[
                'status' => 0,
                'msg' => 'failed'
            ];
        }
        return response()->json($data);
    }

    public function store(Request $request) {
        $userId = Auth::id();
        $measurement = new Measurements;
        $measurement->user_id = $userId;
        $measurement->hand = $request->hand;
        $measurement->span_1_2 = $request->span_1_2;
        $measurement->span_1_3 = $request->span_1_3;
        $measurement->span_1_4 = $request->span_1_4;
        $measurement->span_1_5 = $request->span_1_5;
        $measurement->span_2_3 = $request->span_2_3;
        $measurement->span_2_4 = $request->span_2_4;
        $measurement->span_2_5 = $request->span_2_5;
        $measurement->span_3_4 = $request->span_3_4;
        $measurement->span_3_5 = $request->span_3_5;
        $measurement->span_4_5 = $request->span_4_5;
        $measurement->save();
        return response([
            'message' => 'Meting aangemaakt!',
            'measurement' => $measurement
          ],200);

        // $measurement->height_difference_1_3 = $request->hand;
        // $measurement->height_difference_3_5 = $request->hand;


    }
}
