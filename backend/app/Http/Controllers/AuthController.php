<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class AuthController extends Controller
{


   //  public function testimage(request $request){
   //    if($request->hasFile('image')){
   //      $image = $request->file('image');
   //      // $request->validate([
   //      //      $image => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
   //      //  ]);
   //       $imageName = time().'.'.$image->extension();
   //       $request->image->move(public_path('images'), $imageName);
   //    }
   //
   // }


    //register users
    public function register(Request $request){
      // if($request->hasFile('image')){
      //   $image = $request->file('image');
      //    $imageName = time().'.'.$image->extension();
      //    $request->image->move(public_path('images'), $imageName);
      // }else{
      //   $imageName = '';
      // }

      $image = $this->saveImage($request->avatar_image, 'profiles');


      $attrs = $request->validate([
        'firstname' => 'required|string',
        'middlename' => 'nullable|string',
        'lastname' => 'required|string',
        'sex' => 'required|string|max:7',
        'musical_instrument' => 'nullable|string',
        'date_of_birth' => 'date',
        'email' => 'required|string',
        'email_doctor' => 'nullable|string',
        'avatar_image' => 'nullable|string',
        'password' => 'required|min:6|confirmed'
      ]);


      //create users
      $user = User::create([
        'firstname' => $attrs['firstname'],
        'middlename' => $attrs['middlename'],
        'lastname' => $attrs['lastname'],
        'sex' => $attrs['sex'],
        'musical_instrument' => $attrs['musical_instrument'],
        'date_of_birth' => $attrs['date_of_birth'],
        'email' => $attrs['email'],
        'email_doctor' => $attrs['email_doctor'],
        'avatar_image' => $image ,
        'password' => bcrypt($attrs['password']),
      ]);


      //return user & token
      return response([
        'user' => $user,
        'token' => $user->createToken('secret')->plainTextToken
      ]);
    }

    //login
    public function login(Request $request){
      $attrs = $request->validate([
        'email' => 'required|email',
        'password' => 'required|min:6'
      ]);

      //attempt login
      if(!Auth::attempt($attrs)){
        return response([
          'message' => 'Invalid Credentials'
        ], 403);
      }

      //return user & token
      return response([
        'user' => auth()->user(),
        'token' => auth()->user()->createToken('secret')->plainTextToken
      ],200);
    }


    //logout
    public function logout(){
      auth()->user()->tokens()->delete();
      return response([
        'message' => 'Logout succes'
      ], 200);
    }

    //user details
    public function user(){
      return response ([
        'user' => auth()->user()
      ],200);
    }
}
