<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Http\Controllers\MeasurementController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use Illuminate\Support\Facades\Password;


//public routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

// Route::post('image-upload', [ AuthController::class, 'testimage' ]);


//protected Routes
Route::group(['middleware' => ['auth:sanctum']], function(){

  //user
  Route::get('/user', [AuthController::class, 'user']);
  Route::post('logout', [AuthController::class, 'logout']);
  Route::get('/measurements', [MeasurementController::class, 'index']);

  Route::post('/measurement', [MeasurementController::class, 'show']);
  Route::post('/create-measurement', [MeasurementController::class, 'store']);
  Route::delete('/measurement', [MeasurementController::class, 'delete']);
});


//forgot password
// Route::group(['middleware' => 'web'], function () {

    Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
    Route::group(['middleware' => 'web'], function () {
    Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
  });
    Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');
