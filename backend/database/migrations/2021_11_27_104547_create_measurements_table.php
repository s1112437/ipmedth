<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('hand');
            $table->float('span_1_2');
            $table->float('span_1_3');
            $table->float('span_1_4');
            $table->float('span_1_5');
            $table->float('span_2_3');
            $table->float('span_2_4');
            $table->float('span_2_5');
            $table->float('span_3_4');
            $table->float('span_3_5');
            $table->float('span_4_5');
            // $table->float('height_difference_1_3');
            // $table->float('height_difference_3_5');
            // $table->float('hand_width');
            // $table->float('hand_length');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
