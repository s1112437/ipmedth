<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // DB::table('users')->insert([
        //     'firstname' => Str::random(10),
        //     'lastname' => Str::random(10),
        //     'sex' => 'male',
        //     'musical_instrument' => 'piano',
        //     'date_of_birth' => date('2021-11-29 00:00:00'),
        //     'email' => Str::random(10).'@gmail.com',
        //     'email_doctor' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        // ]);
        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('measurements')->insert([
            'user_id' => 1,
            'hand' => 'Right',
            'span_1_2' => rand(0, 10) / 10,
            'span_1_3' => rand(0, 10) / 10,
            'span_1_4' => rand(0, 10) / 10,
            'span_1_5' => rand(0, 10) / 10,
            'span_2_3' => rand(0, 10) / 10,
            'span_2_4' => rand(0, 10) / 10,
            'span_2_5' => rand(0, 10) / 10,
            'span_3_4' => rand(0, 10) / 10,
            'span_3_5' => rand(0, 10) / 10,
            'span_4_5' => rand(0, 10) / 10,
            'height_difference_1_3' => rand(0, 10) / 10,
            'height_difference_3_5' => rand(0, 10) / 10,
            'hand_width' => rand(0, 20) / 10,
            'hand_length' => rand(0, 20) / 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
