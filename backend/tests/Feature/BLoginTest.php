<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BLoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login()
    {
        # Stuur post request om een user in te loggen
        $response = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }

    public function test_logout(){
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];
        
        # Stuur post request om een user uit te loggen en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->post('/api/logout', []
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }

    public function test_user_details(){
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];
        
        # Stuur post request om de details van een user te krijgen en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->get('/api/user'
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }

}
