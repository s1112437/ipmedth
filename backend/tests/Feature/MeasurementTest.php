<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MeasurementTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_unauthorized()
    {
        # Stuur post request om een meting aan te maken
        $response = $this->withHeaders(['Accept' => 'application/json',])->post('/api/create-measurement',[
            'hand' => 'rechts',
            'span_1_2' => '8.92907046250865',
            'span_1_3' => '7.678851256680405',
            'span_1_4' => '10.615025721013481',
            'span_1_5' => '14.15534502581975',
            'span_2_3' => '1.6818644944756336',
            'span_2_4' => '9.230903459816078',
            'span_2_5' => '10.46147034407688',
            'span_3_4' => '0.5878086612344379',
            'span_3_5' => '6.9257333333333335',
            'span_4_5' => '6.854842565336972'
        ]);

        # Check of vanuit post request statuscode unauthorized terugkomt
        $response->assertUnauthorized();
    }
    public function test_create_measurement()
    {
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];
        
        # Stuur post request om een meting aan te maken en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->post('/api/create-measurement',
        [
            'hand' => 'rechts','span_1_2' => '8.92907046250865',
            'span_1_3' => '7.678851256680405',
            'span_1_4' => '10.615025721013481',
            'span_1_5' => '14.15534502581975',
            'span_2_3' => '1.6818644944756336',
            'span_2_4' => '9.230903459816078',
            'span_2_5' => '10.46147034407688',
            'span_3_4' => '0.5878086612344379',
            'span_3_5' => '6.9257333333333335',
            'span_4_5' => '6.854842565336972'
        ]
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }

    public function test_get_measurements()
    {
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];

        # Stuur post request om een meting aan weer te geven en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->get('/api/measurements'
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }
    public function test_get_measurement()
    {
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];
        # Stuur post request om de details van de aangegeven meting te verkrijgen en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->post('/api/measurement', ['measurementId' => 1]
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }
    public function test_delete_measurement()
    {
        # Stuur post request om een user in te loggen
        $token = $this->post('/api/login', [
            'email' => "pklaasen@test.com",
            'password' => "Welkom01"
        ])['token'];
        # Stuur delete request om aangegeven meting te verwijderen en geef token van user mee om aan te geven welke user is ingelogd
        $response = $this->delete('/api/measurement', ['measurementId' => 1]
        ,['HTTP_Authorization' => 'Bearer '.$token]);
        # Check of vanuit post request statuscode 200 terugkomt
        $response->assertStatus(200);
    }
}
