<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ARegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register()
    {
        # Post request naar register route met een body om een account te registreren
        $response = $this->post('/api/register', [
            'firstname' => "Piet",
            'middlename' => "",
            'lastname' => "Klaasen",
            'sex' => "Male",
            'musical_instrument' => "Piano",
            'date_of_birth' => '1993-09-12',
            'email' => 'pklaasen@test.com',
            'email_doctor' => 'janpeetersen@arts.com',
            'avatar_image' => "",
            'password' => "Welkom01",
            'password_confirmation' => "Welkom01"
        ]);
        #Controleren of er vanaf de post request een 200 statuscode terugkomt
        $response->assertStatus(200);
    }
}


